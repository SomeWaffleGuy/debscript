#/bin/bash
#Sidify: Switch to Debian Sid
echo "$(tput setaf 2)$(tput bold)This script simply switches the sources.list to Debian Sid and updates. Run after the main DebScript for best results. $(tput sgr 0)"
echo -n "$(tput setaf 2)$(tput bold)Continue? 
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	echo "$(tput setaf 2)$(tput bold)Modifying sources.list...$(tput sgr 0)"
		sudo su -c 'echo "# Debian Sid
deb https://deb.debian.org/debian/ sid main contrib non-free
#deb-src https://deb.debian.org/debian/ sid main contrib non-free" > /etc/apt/sources.list'
		sudo apt-get update
		sudo apt-get dist-upgrade -y
fi
exit 0

# DebScript
A better-than-bad Debian post-install script

Should (mostly) work and provide a basic useable system

NOTE: This script assumes you installed the KDE desktop.

### Instructions (Bullseye)

```
wget https://gitlab.com/SomeWaffleGuy/debscript/-/raw/master/debscriptv3.sh
chmod +x debscriptv3.sh
./debscriptv3.sh
```

### Switching to Sid

```
wget https://gitlab.com/SomeWaffleGuy/debscript/-/raw/master/sidify.sh
chmod +x sidify.sh
./sidify.sh
```

# Additional Scripts

If you use Fedora, check out;

https://gitlab.com/SomeWaffleGuy/tippyscript

It includes a README.md with configuration tips helpful for Debian as well.

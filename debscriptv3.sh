#/bin/bash
#DebScript Bullseye: A Better Debian Install Script
echo "$(tput setaf 2)$(tput bold)This script will configure a fresh install of Debian with KDE to be what I consider a useable desktop. This scrtipt will suggest$(tput sgr 0)$(tput setaf 1)$(tput bold) NON-FREE SOFTWARE AND DRIVERS$(tput sgr 0)$(tput setaf 2)$(tput bold) and software which may be subject to restrictions under local law. These choices will be clearly indicated. $(tput sgr 0)"
echo -n "$(tput setaf 2)$(tput bold)Continue? 
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	echo "$(tput setaf 2)$(tput bold)Uninstalling deb-based LibreOffice...$(tput sgr 0)"
		sudo apt-get purge -y libreoffice*
		sudo apt-get autoremove --purge -y
	echo "$(tput setaf 2)$(tput bold)Modifying sources.list...$(tput sgr 0)"
		sudo su -c 'echo "# Debian Bullseye
# Uncommment as needed upon major release
deb https://deb.debian.org/debian/ bullseye main contrib non-free
#deb-src https://deb.debian.org/debian/ bullseye main contrib non-free
deb https://deb.debian.org/debian/ bullseye-updates main contrib non-free
#deb-src https://deb.debian.org/debian/ bullseye-updates main contrib non-free
#deb https://deb.debian.org/debian-security bullseye/updates main contrib non-free
#deb-src https://deb.debian.org/debian-security bullseye/updates main contrib non-free
# Backports (Update codename manually for major version changes)
#deb http://ftp.debian.org/debian bullseye-backports main contrib non-free
#deb-src http://ftp.debian.org/debian bullseye-backports main contrib non-free" > /etc/apt/sources.list'
		sudo apt-get update
		sudo dpkg --add-architecture i386
		sudo apt-get update
		sudo apt-get dist-upgrade -y
	echo "$(tput setaf 2)$(tput bold)Installing necessary components...$(tput sgr 0)"
		sudo apt-get install linux-image-amd64 linux-headers-amd64 firmware-linux-free flatpak -y
	echo -n "$(tput setaf 2)$(tput bold)Install Intel Microcode? $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE$(tput sgr 0)$(tput setaf 2)$(tput bold)
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		sudo apt-get install intel-microcode -y
	fi
	echo -n "$(tput setaf 2)$(tput bold)Install AMD Microcode? $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE$(tput sgr 0)$(tput setaf 2)$(tput bold)
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		sudo apt-get install amd64-microcode -y
	fi
	echo -n "$(tput setaf 2)$(tput bold)Install Intel Network Drivers? $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE$(tput sgr 0)$(tput setaf 2)$(tput bold)
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		sudo apt-get install firmware-iwlwifi -y
	fi
	echo -n "$(tput setaf 2)$(tput bold)Install Realtek Network Drivers? $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE$(tput sgr 0)$(tput setaf 2)$(tput bold)
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		sudo apt-get install firmware-realtek -y
	fi
	sudo apt-get autoremove --purge
	echo -n "$(tput setaf 2)$(tput bold)Which GPU do you have?
1: Intel
2: AMD/ATI $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE$(tput sgr 0)$(tput setaf 2)$(tput bold)
3: Nvidia w/ Nouveau (Not recommended)
4: Nvidia w/ proprietary driver $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE$(tput sgr 0)$(tput setaf 2)$(tput bold)
$(tput sgr 0)"
	read answer
	if echo "$answer" | grep -iq "^1" ;then
		sudo su -c 'echo "# KMS
intel_agp
drm
i915 modeset=1" >> /etc/initramfs-tools/modules'
		elif echo "$answer" | grep -iq "^2" ;then
		sudo apt-get install firmware-amd-graphics -y
		sudo su -c 'echo "# KMS
drm
radeon modeset=1" >> /etc/initramfs-tools/modules'
		elif echo "$answer" | grep -iq "^3" ;then
		sudo su -c 'echo "# KMS
drm
nouveau modeset=1" >> /etc/initramfs-tools/modules'
		elif echo "$answer" | grep -iq "^4" ;then
		sudo su -c 'echo "options nvidia-drm modeset=1" > /etc/modprobe.d/nvidia-drm-nomodeset.conf'
		sudo apt-get install nvidia-driver nvidia-cuda-dev nvidia-cuda-toolkit nvidia-driver-libs:i386 -y
	fi
	echo "$(tput setaf 2)$(tput bold)Installing typical applications...(tput sgr 0)"
		sudo apt-get install -y libavcodec-extra celluloid fonts-cabin fonts-comfortaa fonts-croscore fonts-ebgaramond fonts-ebgaramond-extra fonts-font-awesome fonts-freefont-otf fonts-freefont-ttf fonts-gfs-artemisia fonts-gfs-complutum fonts-gfs-didot fonts-gfs-neohellenic fonts-gfs-olga fonts-gfs-solomos fonts-junicode fonts-lmodern fonts-lobster fonts-lobstertwo fonts-noto-hinted fonts-oflb-asana-math fonts-sil-gentiumplus fonts-sil-gentiumplus-compact fonts-stix fonts-texgyre fonts-arphic-ukai fonts-arphic-uming fonts-ipafont-mincho fonts-ipafont-gothic fonts-unfonts-core fonts-roboto fonts-symbola zip plymouth plymouth-themes plymouth-theme-breeze kde-config-plymouth apparmor apparmor-profiles apparmor-profiles-extra apparmor-utils powertop tlp thermald ffmpegthumbs network-manager-gnome firewall-applet firewall-config firewalld kde-config-tablet neofetch foomatic-db-engine dolphin-nextcloud plasma-browser-integration flashrom plasma-discover-backend-flatpak plasma-discover-backend-fwupd fwupd sddm-theme-breeze vrms
		sudo su -c 'echo "export PLASMA_USE_QT_SCALING=1" >> /etc/profile'
		sudo su -c 'echo "export GTK_USE_PORTAL=1" >> /etc/profile'
		sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	echo -n "$(tput setaf 2)$(tput bold)Install $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE$(tput sgr 0)$(tput setaf 2)$(tput bold) extras?
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		sudo apt-get install -y ttf-mscorefonts-installer unrar steam
	fi
	echo "$(tput setaf 2)$(tput bold)Enabling Plymouth and AppArmor...$(tput sgr 0) "
		sudo perl -pi -e 's,GRUB_CMDLINE_LINUX_DEFAULT="(.*)"$,GRUB_CMDLINE_LINUX_DEFAULT="$1 splash",' /etc/default/grub
		sudo perl -pi -e 's,GRUB_CMDLINE_LINUX="(.*)"$,GRUB_CMDLINE_LINUX="$1 apparmor=1 security=apparmor",' /etc/default/grub
		sudo update-grub
		sudo plymouth-set-default-theme -R bgrt
		sudo update-initramfs -u
	echo -n "$(tput setaf 2)$(tput bold)Replace Firefox-ESR with latest Firefox? It can update itself. Probably don't do this if you intend to switch to Sid.
(y/N)$(tput sgr 0) "
read answer
	if echo "$answer" | grep -iq "^y" ;then
		wget --content-disposition "https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-US"
		tar -xvf firefox*.tar.bz2
		sudo mv firefox /opt/
		sudo ln -s /opt/firefox/firefox /usr/local/bin/firefox
		wget https://gitlab.com/SomeWaffleGuy/debscript/-/raw/master/firefox.desktop
		#chmod maybe not needed? Let me know.
		chmod +x firefox.desktop
		mv firefox.desktop ~/.local/share/applications/
		wget https://gitlab.com/SomeWaffleGuy/debscript/-/raw/master/firefox-equiv.deb
		sudo dpkg -i firefox-equiv.deb
		rm firefox-equiv.deb firefox*.tar.bz2
		sudo apt-get -y purge firefox-esr
		sudo apt-get -y autoremove --purge
		sudo apt-get -y install libdbus-glib-1-2
	fi
	echo -n "$(tput setaf 2)$(tput bold)Install Google Chrome? $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE$(tput sgr 0)$(tput setaf 2)$(tput bold)
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
		sudo dpkg -i google-chrome-stable_current_amd64.deb
		sudo rm google-chrome-stable_current_amd64.deb
		sudo apt-get --fix-broken install -y
	fi
	echo -n "$(tput setaf 2)$(tput bold)Install Lutris? (Not non-free but in contrib)
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		sudo apt-get install -y lutris
	fi
	echo -n "$(tput setaf 2)$(tput bold)Install BlueMaxima's Flashpoint (will also install 32-bit Wine and PHP if not installed)? $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE$(tput sgr 0)$(tput setaf 2)$(tput bold) 
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		wget https://bluepload.unstable.life/selif/flashpoint-infinity-8-2-amd64-deb.7z
		7z x flashpoint-infinity-8-2-amd64-deb.7z
		sudo dpkg -i flashpoint*.deb
		rm flashpoint*.7z flashpoint*.deb
		sudo apt-get --fix-broken install -y
		sudo mkdir /opt/flashpoint-infinity
		sudo chown $(whoami) /opt/flashpoint-infinity
		echo "$(tput setaf 2)$(tput bold)Flashpoint requires setup on first run. I suggest using the created /opt/flashpoint-infinity for this. Additional configuration is found in /usr/lib/flashpoint-infinity/config.json  $(tput sgr 0) "
	fi
	echo -n "$(tput setaf 2)$(tput bold)Install latest adb/fastboot? 
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		sudo apt-get -y install git
		git clone https://github.com/M0Rf30/android-udev-rules.git
		sudo cp ./android-udev-rules/51-android.rules /etc/udev/rules.d/51-android.rules
		sudo chmod a+r /etc/udev/rules.d/51-android.rules
		sudo groupadd adbusers
		sudo usermod -a -G adbusers $(whoami)
		sudo systemctl restart systemd-udevd.service
		wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
	unzip platform-tools-latest-linux.zip
		sudo cp platform-tools/adb platform-tools/fastboot platform-tools/mke2fs* platform-tools/e2fsdroid /usr/local/bin
		sudo rm -rf platform-tools platform-tools-latest-linux.zip
		wget https://gitlab.com/SomeWaffleGuy/debscript/-/raw/master/adb-updater.sh
		chmod +x adb-updater.sh
		#Better method of update?
		echo "$(tput setaf 2)$(tput bold)Use the included adb-updater.sh script to update adb/fastboot as needed. $(tput sgr 0) "
	fi
	echo -n "$(tput setaf 2)$(tput bold)Install latest youtube-dl? 
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		sudo apt-get -y install curl
		sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
		sudo chmod a+rx /usr/local/bin/youtube-dl
	fi
	echo -n "$(tput setaf 2)$(tput bold)Set Pulseaudio to s24le (Improves quality at slight peformance cost)? 
(y/N)$(tput sgr 0) "
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		sudo sed -i "s/; default-sample-format = s16le/default-sample-format = s24le/g" /etc/pulse/daemon.conf
		sudo sed -i "s/; resample-method = speex-float-1/resample-method = speex-float-10/g" /etc/pulse/daemon.conf
		sudo sed -i "s/; avoid-resampling = false/avoid-resampling = true/g" /etc/pulse/daemon.conf
	fi	
echo "$(tput setaf 2)$(tput bold)Setup complete, please restart your system. If you find a device is not working, try using the firmware-misc-nonfree package. If all else fails, install the much more broad firmware-linux package. These are both $(tput sgr 0)$(tput setaf 1)$(tput bold)NON-FREE.$(tput sgr 0)$(tput setaf 2)$(tput bold)$(tput sgr 0)"
fi
exit 0
